const donations = (state = {}, action) => {
  switch (action.type) {
    case 'DONATIONS_REQUESTED':
    case 'DONATIONS_REQUEST_SUCCESS':
      return Object.assign({}, state, action.payload);
    default:
      return state;
  }
};

export default donations;
