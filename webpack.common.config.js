module.exports = {
  devtool: 'source-map',
  module: {
    loaders: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loader: 'babel',
        query: {
          presets: ['es2015', 'react'],
          babelrc: false,
        },
      },
      {
        test: /\.html$/,
        loader: 'html',
      },
      {
        test: /\.json$/,
        loader: 'json-loader',
      },
    ],
  },
  htmlLoader: {
    attrs: false,
  },
  resolve: {
    extensions: ['', '.js', '.jsx'],
  },
};
