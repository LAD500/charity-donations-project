import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import combinedreducer from './reducers/combined';
import DonationsPane from './containers/donations-pane';


const store = createStore(
  combinedreducer,
  applyMiddleware(thunk),
);

export default function init() {
  ReactDOM.render(
    (
      <Provider store={store}>
        <DonationsPane />
      </Provider>
    ),
    document.getElementById('main'));
}
