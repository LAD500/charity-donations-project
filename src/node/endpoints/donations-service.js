import configuration from './../config/configuration';
import requestJSON from './../../universal/network/request-json';
import buildDonations from './../builders/build-donations';

export default function donationsService(req, res) {
  const charityId = req.params.charityId;
  const urlBase = configuration.retrieve('url')['just-giving'].base;
  const appId = configuration.retrieve('appId');

  res.setHeader('Content-Type', 'application/json');

  Promise.all([
    requestJSON(`${urlBase}/${appId}/v1/charity/${charityId}`),
    requestJSON(`${urlBase}/${appId}/v1/charity/${charityId}/donations`),
  ]).then(buildDonations)
    .then(builtData => res.status(200).json(builtData))
    .catch(err => res.status(400).json({ err: err.message }));
}
