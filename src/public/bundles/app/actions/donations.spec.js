import { expect } from 'chai';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import sinon from 'sinon';
import mockery from 'mockery';
import { donationsRequested, donationsRequestSuccess } from './donations';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('requestDonations', () => {
  let sandbox;

  before(() => {
    sandbox = sinon.sandbox.create();
  });

  beforeEach(() => {
    mockery.enable({
      warnOnReplace: false,
      warnOnUnregistered: false,
      useCleanCache: true,
    });
  });

  afterEach(() => {
    mockery.deregisterAll();
    mockery.disable();
    sandbox.restore();
  });

  it('creates request success actions when request is successful', () => {
    const mockRequestPromise = new Promise(resolve => resolve({ some: ['json'] }));
    const requestJsonStub = sandbox.stub().returns(mockRequestPromise);
    mockery.registerMock('./../../../../universal/network/request-json',
                    requestJsonStub);

    const expectedActions = [
      {
        type: 'DONATIONS_REQUESTED',
        payload: {
          responseReceived: false,
        },
      },
      {
        type: 'DONATIONS_REQUEST_SUCCESS',
        payload: {
          responseReceived: true,
          body: { some: ['json'] },
        },
      },
    ];

    const store = mockStore({ donations: {} });

    const requestDonations = require('./donations').requestDonations; // eslint-disable-line

    return store.dispatch(requestDonations('charityId'))
      .then(() => {
        expect(store.getActions()).to.deep.equal(expectedActions);
      });
  });
});

describe('donationsRequested', () => {
  it('should be return the correct action object', () => {
    expect(donationsRequested()).to.deep.equal({
      type: 'DONATIONS_REQUESTED',
      payload: {
        responseReceived: false,
      },
    });
  });
});

describe('donationsRequestSuccess', () => {
  it('should be return the correct action object with the body appended', () => {
    const body = { some: 'json' };

    expect(donationsRequestSuccess(body)).to.deep.equal({
      type: 'DONATIONS_REQUEST_SUCCESS',
      payload: {
        responseReceived: true,
        body,
      },
    });
  });
});
