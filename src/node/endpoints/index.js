import donationsService from './donations-service';

export default function endpoints(app) {
  app.get('/charity/:charityId/donations', donationsService);
}
