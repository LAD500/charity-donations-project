import { combineReducers } from 'redux';
import donations from './donations';

const app = combineReducers({
  donations,
});

export default app;
