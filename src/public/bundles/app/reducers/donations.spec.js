import { expect } from 'chai';
import donations from './donations';

describe('donations reducer', () => {
  it('should initialise the state', () => {
    expect(donations(undefined, {})).to.deep.equal({});
  });
  it('should return welformed state with action type of DONATIONS_REQUESTED', () => {
    const action = {
      type: 'DONATIONS_REQUESTED',
      payload: {
        responseReceived: false,
      },
    };

    expect(donations(undefined, action)).to.deep.equal({
      responseReceived: false,
    });
  });

  it('should return wellformed state with action type of DONATIONS_REQUEST_SUCCESS', () => {
    const action = {
      type: 'DONATIONS_REQUEST_SUCCESS',
      payload: {
        responseReceived: true,
        body: { donations: 'info here' },
      },
    };

    expect(donations(undefined, action)).to.deep.equal({
      responseReceived: true,
      body: { donations: 'info here' },
    });
  });

  it('should return wellformed state with action type of DONATIONS_REQUESTED retain parts of previous state', () => {
    const action = {
      type: 'DONATIONS_REQUESTED',
      payload: {
        responseReceived: false,
      },
    };
    const previousState = {
      responseReceived: true,
      body: { donations: 'info here' },
    };

    expect(donations(previousState, action)).to.deep.equal({
      responseReceived: false,
      body: { donations: 'info here' },
    });
  });

  it('should return wellformed state with action type of DONATIONS_REQUEST_SUCCESS overwriting state', () => {
    const action = {
      type: 'DONATIONS_REQUEST_SUCCESS',
      payload: {
        responseReceived: true,
        body: { donations: 'even more info here' },
      },
    };
    const previousState = {
      responseReceived: false,
      body: { donations: 'info here' },
    };

    expect(donations(previousState, action)).to.deep.equal({
      responseReceived: true,
      body: { donations: 'even more info here' },
    });
  });
});
