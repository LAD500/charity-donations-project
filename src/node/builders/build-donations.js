import { getSymbolFromCurrency } from 'currency-symbol-map';

const amountInCurrency = (donation) => {
  let amount;

  if (donation.currencyCode && !isNaN(donation.amount)) {
    amount = `${getSymbolFromCurrency(donation.currencyCode)}${donation.amount}`;
  }

  return amount;
};

const processDateString = (donation) => {
  let dateTime;

  if (donation.donationDate) {
    const stripedOfDate = donation.donationDate.match(/\((.*)\)/i)[1];
    const seperatorIndex = stripedOfDate.indexOf('+');
    const milliseconds = parseInt(stripedOfDate.substring(0, seperatorIndex), 10);
    const timezoneHours = parseInt(stripedOfDate.substring(seperatorIndex + 1, seperatorIndex + 3)
        , 10) * 60 * 1000;
    const timezoneMinutes = parseInt(stripedOfDate.substring(seperatorIndex + 3, seperatorIndex + 5)
        , 10) * 1000;
    const dateTimeSum = milliseconds + timezoneHours + timezoneMinutes;
    if (!isNaN(dateTimeSum)) dateTime = new Date(dateTimeSum).toJSON();
  }

  return dateTime;
};

const covertedDonations = donations => donations.map(donation => ({
  amount: amountInCurrency(donation),
  donationDate: processDateString(donation),
  donorDisplayName: donation.donorDisplayName,
  imageUrl: donation.imageUrl,
  message: donation.message,
}));

export default function donationsBuilder(results) {
  const charityData = results[0];
  const donationsData = results[1].donations;

  return {
    name: charityData.name,
    logoUrl: charityData.logoAbsoluteUrl,
    justGivingPageUrl: charityData.profilePageUrl,
    donations: covertedDonations(donationsData),
  };
}
