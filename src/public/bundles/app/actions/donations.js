import requestJson from './../../../../universal/network/request-json';

export const donationsRequested = () => ({
  type: 'DONATIONS_REQUESTED',
  payload: {
    responseReceived: false,
  },
});

export const donationsRequestSuccess = body => ({
  type: 'DONATIONS_REQUEST_SUCCESS',
  payload: {
    responseReceived: true,
    body,
  },
});

export const requestDonations = charityId => (dispatch) => {
  dispatch(donationsRequested());
  return requestJson(`/charity/${charityId}/donations`)
    .then(json => dispatch(donationsRequestSuccess(json)));
};
