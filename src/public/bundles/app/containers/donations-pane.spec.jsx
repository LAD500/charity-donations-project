import { expect } from 'chai';
import sinon from 'sinon';
import mockery from 'mockery';
import React from 'react';
import jsdom from 'jsdom';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import combinedreducer from './../reducers/combined';

const doc = jsdom.jsdom('<!doctype html><html><body></body></html>');
global.document = doc;
global.window = doc.defaultView;

const store = createStore(
  combinedreducer,
  applyMiddleware(thunk),
);

describe('DonationsPane', () => {
  let sandbox;

  before(() => {
    sandbox = sinon.sandbox.create();
  });

  beforeEach(() => {
    mockery.enable({
      warnOnReplace: false,
      warnOnUnregistered: false,
      useCleanCache: true,
    });
  });

  afterEach(() => {
    mockery.deregisterAll();
    mockery.disable();
    sandbox.restore();
  });

  it('should successfully render and mount', () => {
    const mockRequestPromise = new Promise(resolve => resolve({ some: ['json'] }));
    const requestJsonStub = sandbox.stub().returns(mockRequestPromise);
    mockery.registerMock('./../../../../universal/network/request-json',
      requestJsonStub);

    const DonationsPane = require('./donations-pane').default; // eslint-disable-line global-require
    sinon.spy(DonationsPane.prototype, 'componentDidMount');

    const donationsPane = mount(
      <Provider store={store}>
        <DonationsPane
          donations={{ responseReceived: false }}
        />
      </Provider>);

    expect(donationsPane.find('#donations-pane').length).to.equal(1);
    expect(requestJsonStub.calledOnce).to.be.true;
  });
});
