import React from 'react';

const DonationsHeader = ({ logoUrl, charityName, justGivingPageUrl }) => (
  <div className="donations--header">
    <img className="donations--header--logo" src={logoUrl} alt={charityName} />
    <a className="donations--header--link" href={justGivingPageUrl}>Find out more about {charityName} at JustGiving..</a>
  </div>
);

DonationsHeader.propTypes = {
  logoUrl: React.PropTypes.string,
  charityName: React.PropTypes.string,
  justGivingPageUrl: React.PropTypes.string,
};


export default DonationsHeader;
