import configuration from './config/configuration';
import server from './httpapp';
import endpoints from './endpoints';

process.title = 'charity-donations-node';

configuration.init().then(() => {
  endpoints(server.app);
  server.start(3000, 'build/public/');
});

