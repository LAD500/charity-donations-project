import React from 'react';
import DonationsListItem from './donations-list-item';

const DonationsList = ({ donations }) => (
  <div className="donations--list">
    {
      donations && donations.length ?
        donations.map((donation, i) => (<DonationsListItem {...donation} key={i} />))
        :
        <p className="donations--list--message">Fetching donations</p>
    }
  </div>
);

DonationsList.propTypes = {
  donations: React.PropTypes.array, // eslint-disable-line react/forbid-prop-types
};

export default DonationsList;
