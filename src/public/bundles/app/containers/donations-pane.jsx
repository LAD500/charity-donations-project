import React from 'react';
import { connect } from 'react-redux';
import { requestDonations } from './../actions/donations';
import DonationsHeader from './../components/donations-header';
import DonationsList from './../components/donations-list';

class DonationsPane extends React.Component {
  componentDidMount() {
    this.props.dispatch(requestDonations(13441));
  }

  render() {
    const { donations } = this.props;
    const { body } = donations;

    return (
      <div id="donations-pane" className="donations-pane row">
        { body ?
          <div className=".col-md-8 .col-md-offset-2">
            <DonationsHeader
              logoUrl={body.logoUrl}
              charityName={body.name}
              justGivingPageUrl={body.justGivingPageUrl}
            />
            <DonationsList
              donations={body.donations}
            />
          </div>
        :
          <p className="donations-pane--loading-message">loading</p>
        }
      </div>
    );
  }
}

DonationsPane.propTypes = {
  dispatch: React.PropTypes.func.isRequired,
  donations: React.PropTypes.shape({
    responseReceived: React.PropTypes.bool,
  }),
};

const mapStateToProps = state => ({
  donations: Object.assign({}, state.donations),
});

export default connect(
  mapStateToProps,
)(DonationsPane);
