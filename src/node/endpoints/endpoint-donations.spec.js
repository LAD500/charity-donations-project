import { expect } from 'chai';
import request from 'supertest';
import nock from 'nock';
import server from './../httpapp';
import { charityData, donationsData, expectedOutput } from './endpoint-donations.spec.data';
import endpoints from './../endpoints';
import configuration from './../config/configuration';

describe('endpoint-donations', () => {
  let originalAppId;

  before(() => {
    originalAppId = process.env.APP_ID ? process.env.APP_ID : '';
    process.env.APP_ID = 'asdf1234';
  });

  after(() => {
    process.env.APP_ID = originalAppId;
  });

  it('returns a status of 200 with the correct content', (done) => {
    configuration.init().then((config) => {
      const charityId = 13441;
      const urlBase = config.url['just-giving'].base;
      const appId = config.appId;

      endpoints(server.app);

      nock(urlBase)
        .get(`/${appId}/v1/charity/${charityId}`)
        .reply(200, charityData);

      nock(urlBase)
        .get(`/${appId}/v1/charity/${charityId}/donations`)
        .reply(200, donationsData);

      request(server.app)
        .get(`/charity/${charityId}/donations`)
        .end((err, res) => {
          expect(res.statusCode).to.equal(200);
          expect(res.body).to.deep.equal(expectedOutput);

          server.stop();
          done();
        });
    });
  });
});
