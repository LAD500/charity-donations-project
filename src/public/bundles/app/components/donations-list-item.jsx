import React from 'react';

const formatDate = dateString => new Date(dateString).toLocaleString('en-GB');

const DonationsHeader = ({ amount, donationDate, donorDisplayName, imageUrl, message }) => (
  <div className="donations--list--item">
    <img className="donations--list--item--img" src={imageUrl} alt={donorDisplayName} />
    <h3 className="donations--list--item--donor-name">Donation from {donorDisplayName}</h3>
    {
      (amount || donationDate) &&
      <h4 className="donations--list--item--donation-details">
        {
          amount && <span className="donations--list--item--donation-amount">Gave {amount}. </span>
        }
        {
          donationDate &&
          <span className="donations--list--item--donation-date">
            Donated on <time dateTime={donationDate}>{formatDate(donationDate)}</time>
          </span>
        }
      </h4>
    }
    <p className="donations--list--item--donor-message">
      Donor message: { message }
    </p>
  </div>
);

DonationsHeader.propTypes = {
  amount: React.PropTypes.string,
  donationDate: React.PropTypes.string,
  donorDisplayName: React.PropTypes.string.isRequired,
  imageUrl: React.PropTypes.string.isRequired,
  message: React.PropTypes.string.isRequired,
};

export default DonationsHeader;
